﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckSpawner : MonoBehaviour
{
    public GameObject _duck;
        
    void Start()
    {
        Instantiate(_duck, transform.position, Quaternion.identity);
    }
}

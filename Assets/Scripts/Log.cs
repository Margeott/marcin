﻿
using UnityEngine;

public class Log : MonoBehaviour
{
    public float speed;
    private Transform _transform;

    private void Start()
    {
        _transform = GetComponent<Transform>();
        speed = Random.Range(7.0f, 13.0f);
    }

    void Update()
    {
        _transform.position += speed * Time.deltaTime * Vector3.right;
    }
}

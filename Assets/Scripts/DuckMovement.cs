﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckMovement : MonoBehaviour
{
    Transform DuckTransform;
    public float Duckforspeed;
    void Start()
    {
        DuckTransform = GetComponent<Transform>();
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            DuckTransform.forward = Vector3.forward;
            DuckTransform.position += DuckTransform.forward * Time.deltaTime * Duckforspeed;

        }

        else if (Input.GetKey(KeyCode.S))
        {
            DuckTransform.forward = Vector3.back;
            DuckTransform.position += DuckTransform.forward * Time.deltaTime * Duckforspeed;
        }

        else if (Input.GetKey(KeyCode.D))
        {
            DuckTransform.forward = Vector3.right;
            DuckTransform.position += DuckTransform.forward * Time.deltaTime * Duckforspeed;
        }

        else if (Input.GetKey(KeyCode.A))
        {
            DuckTransform.forward = Vector3.left;
            DuckTransform.position += DuckTransform.forward * Time.deltaTime * Duckforspeed;
        }
    }
}

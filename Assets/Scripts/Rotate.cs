﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
    private Transform _transform;
    
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _transform.eulerAngles += new Vector3(0, 60, 0) * Time.deltaTime;
        }
    }
}

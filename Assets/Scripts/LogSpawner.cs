﻿
using UnityEngine;

public class LogSpawner : MonoBehaviour
{
    public GameObject _log;
    private float _timer;
    private Vector3 _spawnPosition;

    private void Start()
    {
        _spawnPosition = transform.position;
    }

    void Update()
    {
        _timer -= Time.deltaTime;
        
        if (_timer < 0.0f) 
        {
            Instantiate(_log, _spawnPosition, Quaternion.identity);
            _timer += Random.Range(2.0f, 3.0f);
        }
    }
}
